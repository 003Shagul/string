// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}


function string4(file) {
    let array = []
    for (let key in file) {
        let word = file[key];
        firstLetter = word[0].toUpperCase();

        remainingLetter = (word.slice(1, word.length)).toLowerCase();

        let convertedName = firstLetter + remainingLetter;
        array.push(convertedName);
    }
    return array.join(" ");
}



module.exports = string4

